# Assignment Bitvavo - SpaceX Ships

To use the application follow these instructions:

1) Clone repo by running 'git clone https://bitbucket.org/thevinx/assignment-bitvavo.git'<br>
2) Open the new folder<br>
3) Install, build and serve the app by running 'npm run deploy'<br>
4) "Enjoy"
