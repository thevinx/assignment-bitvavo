export type SpacexShipsResult = {
  docs: SpacexShip[]
  hasNextPage: boolean
  hasPrevPage: boolean
  limit: number
  nextPage: number
  page: number
  pagingCounter: number
  prevPage: number
  totalDocs: number
  totalPages: number
}

export interface SpacexShip {
  abs	: number
  active	: boolean
  attempted_landings	: number
  class	: number
  course_deg	: number
  home_port	: string
  id : string
  image	: string
  imo	: number
  launches : string[]
  missions : {
    flight : string
    name : string
  }[]
  mmsi	: number
  model	: string
  name	: string
  position	: {
    latitude : number
    longitude : number
  }
  roles	: string[]
  speed_kn :	number
  status	: string
  successful_landings	: number
  type	: string
  url	: string
  weight_kg	: number
  weight_lbs	: number
  year_built	: number
}