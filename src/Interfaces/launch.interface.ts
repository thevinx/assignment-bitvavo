export interface SpacexLaunchResult {
  data: SpacexLaunch
}

export interface SpacexLaunch {
  auto_update: boolean
  capsules: string[]
  cores: any[]
  crew: []
  date_local: string
  date_precision: string
  date_unix: number
  date_utc: string
  details: string
  failures: []
  fairings: string
  flight_number: number
  id: string
  launch_library_id: string
  launchpad: string
  links: {
    wikipedia: string
  }
  name: string
  net: false
  payloads: string[]
  rocket: string
  ships: string[]
  static_fire_date_unix: number
  static_fire_date_utc: string
  success: boolean
  tbd: boolean
  upcoming: boolean
  window: number
}