import { useEffect, useState } from 'react';
import { Button, Container, TablePagination, TableSortLabel, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
import { SpacexShip, SpacexShipsResult } from '../../../Interfaces/ships.interface';
import classes from './ShipsList.module.css';
import { SortDirection } from '../Ships';

type ShipsListParams = {
  shipsResult:SpacexShipsResult,
  onChangeList:(page:number,sort:string,sortDirection:SortDirection)=>void,
  onViewShipDetails:(ship:SpacexShip)=>void
}

/** Component that handles the ships list table */
const ShipsList = ({ shipsResult, onChangeList, onViewShipDetails }:ShipsListParams) => {

  const headers:string[] = ["name", "type", "active", "year_built", "home_port"];
  const [ ships, setShips ] = useState<SpacexShip[]>([]);
  const [ page, setPage ] = useState<number>(0);
  const [ sort, setSort ] = useState<string>("name");
  const [ sortDirection, setSortDirection ] = useState<SortDirection>(SortDirection.Ascending);

  useEffect(() => {
    const ships = shipsResult?.docs as SpacexShip[];
    setShips(ships);
  }, [shipsResult])

  /** Handles the click of an header column, 
   * setting the sort name and the sort direction */
  const onClickHeader = (header:string) => {
    let newSortDirection:SortDirection = SortDirection.Ascending;

    if ( sort === header ) 
      newSortDirection = sortDirection === SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending;

    setSortDirection(newSortDirection);
    setSort(header);

    onChangeList(page, header, newSortDirection);
  };

  /** Handles the table page change */
  const onChangePage = (event:React.MouseEvent<HTMLButtonElement, MouseEvent> | null, newPage:number):void => {
    setPage(newPage);
    onChangeList(newPage, sort, sortDirection);
  }

  /** Returns the table headers element */
  const renderHeaders = ():JSX.Element[] => {
    return headers.map((header:string,index:number) => 
      <TableCell key={index}>
        <TableSortLabel 
          active={sort === header} 
          direction={sortDirection === 'asc' ? 'asc' : 'desc'} 
          onClick={() => onClickHeader(header)}>{header}
        </TableSortLabel>
      </TableCell>
    );
  }

  /** Returns the table rows elements */
  const renderTable = ():JSX.Element[] => {
    return ships.map(ship => 
      <TableRow key={ship.id}>
        {
          headers.map((header:string,index:number) =>
            <TableCell key={index}>
              {ship[header as keyof SpacexShip]}
            </TableCell>
          )
        }
        <TableCell>
          <Button 
            variant="contained" 
            color="primary"
            onClick={()=>onViewShipDetails(ship)}>
            View
          </Button>
        </TableCell>
      </TableRow>
    );
  }

  return(
    <Container className={classes.TableWrap}>
      <Table color="inherit">
        <TableHead>
          <TableRow>
            { renderHeaders() }
            <TableCell>Details</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          { renderTable() }
        </TableBody>
      </Table>
      
      <TablePagination
        component="div"
        rowsPerPageOptions={[shipsResult.limit]}
        count={shipsResult.totalDocs}
        rowsPerPage={shipsResult.limit}
        page={shipsResult.page ? shipsResult.page - 1 : 0}
        onChangePage={onChangePage}
        />

    </Container>
  );
}

export default ShipsList;