import { useState, useEffect } from 'react';
import { Button, Container, Grid, Typography } from '@material-ui/core';
import axios from '../../endpoints';
import ShipsList from './shipsList/ShipsList';
import ShipsDetails from './shipsDetails/ShipsDetails';
import { SpacexShip, SpacexShipsResult } from '../../Interfaces/ships.interface';
import classes from './Ships.module.css'

type ShipsParams = {
  onLogout:()=>void
}

export enum SortDirection {
  Ascending = 'asc',
  Descending = 'desc'
}

/** Component that handles the ships page */
function Ships({ onLogout }:ShipsParams) {

  const [ shipsResult, setShipsResult ] = useState<SpacexShipsResult>({
    docs: [],
    hasNextPage: false,
    hasPrevPage: false,
    limit: 0,
    nextPage: 0,
    page: 0,
    pagingCounter: 0,
    prevPage: 0,
    totalDocs: 0,
    totalPages: 0,
  });

  const [ selectedShip, setSetlectedShip ] = useState<SpacexShip|null>(null);

  /** Gets the ships data from api, 
   * according to search params:
   * @param page current page number
   * @param sort column name to sort
   * @param sortDirection sort direction, either ascending (asc) or descending (desc)
   */
  const getShips = (page:number = 0, sort:string = "name", sortDirection:SortDirection = SortDirection.Ascending):void => {
    const params = {
      options: {
        page: page + 1,
        sort: {
          [sort]: sortDirection
        }
      }
    }

    axios.post('/ships/query', params)
      .then(resp => setShipsResult(resp.data))
      .catch(error => console.error(error));
  };

  /** Handles the changes in the list interaction */
  const onChangeList = (page:number, sort:string, sortDirection:SortDirection):void => {
    getShips(page, sort, sortDirection);
  }

  /** Handles the click on the view button of a ship record */
  const onViewShipDetails = (ship:SpacexShip):void => {
    setSetlectedShip(ship);
  }

  useEffect(() => {
    getShips();
  }, [])
  
  return(
    <Container 
      component="main" 
      maxWidth="xl" 
      className={classes.Ships}>

      <Grid container spacing={2}>

        <Grid item xs={12}>
          <img src="./spacex-logo.svg" alt="logo" className={classes.Logo} />
        </Grid>
        
        <Grid container spacing={2} item xs={12}>
          <Grid item xs={9}>
            <div className={classes.HeaderWrap}>
              <Typography align="left" variant="h4" className={classes.HeaderTitle}>SpaceX Ships</Typography>
              <Typography align="left" variant="subtitle1" className={classes.HeaderSubtitle}>Detailed info about ships in the SpaceX fleet</Typography>  
            </div>

          </Grid>

          <Grid item xs={3}>  
            <div className={classes.ButtonWrap}>
              <Button 
                variant="contained" 
                color="primary" 
                type="button"
                onClick={onLogout}>
                Log Out
              </Button>
            </div>
          </Grid>
        </Grid>

        <Grid item xs={6}>
          <ShipsList 
            shipsResult={shipsResult} 
            onChangeList={onChangeList} 
            onViewShipDetails={onViewShipDetails} />
        </Grid>

        <Grid item xs={6}>
          <ShipsDetails selectedShip={selectedShip} />
        </Grid>

      </Grid>

    </Container>
  );
}

export default Ships;