import { useCallback, useEffect, useState } from 'react';
import { Container, Link, List, ListItem, Typography } from '@material-ui/core';
import axios from '../../../endpoints';
import classes from './ShipsDetails.module.css';
import { SpacexShip } from '../../../Interfaces/ships.interface';
import { SpacexLaunch, SpacexLaunchResult } from '../../../Interfaces/launch.interface';

type ShipsDetailsParams = {
  selectedShip:SpacexShip|null
}

/** Component that handles the ships details view */
const ShipsDetails = ({ selectedShip }:ShipsDetailsParams) => {

  const [ ship, setShip ] = useState<SpacexShip|null>(null);
  const [ launchesDetails, setLaunchesDetails ] = useState<SpacexLaunch[]>([]);

  const placeholderImage:string = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/HMS_Example_%28P165%29_Helsinki.JPG/1200px-HMS_Example_%28P165%29_Helsinki.JPG';

  /** Gets a single launch data given the launch id
   * @param id launch id
   * @returns a promise of launch details
   */
  const getLaunch = (id:string):Promise<void | SpacexLaunch> => {
    return axios.get(`/launches/${id}`)
      .then((resp:SpacexLaunchResult) => resp.data as SpacexLaunch)
      .catch(error => console.error(error));
  };

  /** Gets all the launches given the array of launch ids
   * @param launchesIds launch ids array
   */
  const getAllLaunches = useCallback( 
    async (launchesIds:string[]):Promise<(void | SpacexLaunch)[]> => 
      await Promise.all(launchesIds.map((id:string) => getLaunch(id))),
    []
  );

  useEffect(() => {
    if ( !selectedShip ) return;

    getAllLaunches(selectedShip.launches)
      .then(resp => {
        setLaunchesDetails(resp as SpacexLaunch[]);
        setShip(selectedShip);
      });
  }, [selectedShip, getAllLaunches]);

  /** Returns the ship details element */
  const renderShip = ():JSX.Element => {
    return (
      <Container>
        <Typography variant="h6" align="left">
          { ship?.name } { ship?.year_built ? `(${ship?.year_built})` : null }          
        </Typography>
        <Typography variant="body1" align="left">
          {ship?.roles}
        </Typography>
        <img 
          className={classes.ShipImage}  
          alt={
            ship?.name ?? "Example ship"
          }
          src={
            ship?.image ?? placeholderImage
          } />
          { 
            ship?.launches.length ?  
              renderLaunches() :
              <Typography align="left">No launches</Typography>
          }
      </Container>
    );
  }

  /** Returns the list of launches element */
  const renderLaunches = ():JSX.Element => {
    return (
      <Container>
        <Typography align="left">Launches</Typography>
        <List className={classes.List}>
          {
            launchesDetails.map((launchDetails:SpacexLaunch, i:number) => (
                <ListItem button key={i}>
                  <Link href={ launchDetails?.links?.wikipedia } target="_blank">
                    { launchDetails?.name }
                  </Link>
                </ListItem>  
              )
            )
          }          
        </List>
      </Container>
    );
  }

  /** Returns the image placeholder element */
  const renderPlaceholder = ():JSX.Element => {
    return (
      <Container>
        <Typography variant="h6">Select a ship to view</Typography>
      </Container>
    );
  }

  return(
    <div className={classes.SectionWrap}>
      { ship ? renderShip() : renderPlaceholder() }
    </div>
  );
}

export default ShipsDetails;