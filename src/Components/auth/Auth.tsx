import { useState, useEffect } from 'react';
import { Route } from 'react-router';
import { Switch, useHistory } from 'react-router-dom';
import AuthGuard from './AuthGuard';
import Login from '../login/Login';
import Ships from '../ships/Ships';

export type CredentialsType = {
  email:string
  password:string
}

/** Component that takes care of handling the authorization
 * and routing of the app. */
const Auth = () => {

  const storageKey:string = "app-auth",
        adminEmail:string = "email@test.com",
        adminPassword:string = "test123";

  const history = useHistory();
  
  const [auth, setAuth] = useState<boolean>(false);

  const errorMessage:string = "Wrong email or password";
  const [ error, setError ] = useState<string>("");

  /** Saves credentials to local storage */
  const setAuthInStorage = (credentials:CredentialsType):void => {
    localStorage.setItem(storageKey, JSON.stringify(credentials));
  }

  /** Checks if email and password are valid, if so, routes to ships page  */
  const checkCredentials = (cred:CredentialsType, rememberMe:boolean = false):void => {
    if ( cred.email === adminEmail && cred.password === adminPassword ) {
      setAuth(true);
      setError("");
      if ( rememberMe ) setAuthInStorage(cred);

      history.push('/ships');
    } 
    else {
      setAuth(false);
      setError(errorMessage);
    }
  }

  /** Loads credentials for local storage if any */
  const loadCredentials = ():void => {
    const getAuthFromStorage = ():CredentialsType|null => {
      const authFromStorage:string|null = localStorage.getItem(storageKey);
      
      if ( !authFromStorage ) return null;
      else return JSON.parse(authFromStorage as string);
    }

    const credentials:CredentialsType|null = getAuthFromStorage();
    if ( !credentials ) return;

    checkCredentials(credentials);
  }

  useEffect(loadCredentials);

  /** Handles the login action */
  const onLogin = (cred:CredentialsType,rememberMe:boolean):void => {
    checkCredentials(cred, rememberMe);
  }

  /** Handles the logout action, routing to login page and clears local storage */
  const onLogout = ():void => {
    history.push('/login');
    localStorage.clear();
  }
  
  /** Handles the credential field changes, resetting the error */
  const onChangeInput = (cred:CredentialsType):void => {
    setError("");
  }

  return(
    <Switch>
      <Route path="/login">
        <Login onLogin={onLogin} onChangeInput={onChangeInput} error={error} />
      </Route>
      <AuthGuard path={['/ships', '/']} isAuthorized={auth}>
        <Ships onLogout={onLogout} />
      </AuthGuard>
    </Switch>
  );
}

export default Auth;