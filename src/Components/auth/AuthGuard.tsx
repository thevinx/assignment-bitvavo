import { Redirect, Route } from 'react-router';

type AuthGuardParams = { 
  children:React.ReactNode, 
  path:string|string[], 
  isAuthorized:boolean 
}

/** Component that checks if user is authorized before loading a component, 
 * otherwise routes to login */
const AuthGuard = ({ children, path, isAuthorized }:AuthGuardParams) => {
  return(
    <Route>
      { 
        isAuthorized ? 
          children :
          <Redirect to={{ pathname: "/login" }} />
      }
    </Route>
  )
}

export default AuthGuard;