import React, { useState } from 'react';
import { CredentialsType } from '../auth/Auth';
import { Button, Checkbox, Container, FormControlLabel, TextField, Typography } from '@material-ui/core';
import classes from './Login.module.css';

type LoginParams = {
  onLogin:(cred:CredentialsType,rememberMe:boolean)=>void
  onChangeInput:(cred:CredentialsType)=>void
  error:string
}

/** Component that handles the login page and the login form */
function Login({ onLogin, onChangeInput, error }:LoginParams) {

  const [ cred, setCred ] = useState<CredentialsType>({
    email: "",
    password: ""
  });

  const [ rememberMe, setRememberMe ] = useState<boolean>(false);

  /** Sets the email value on change */
  const onChangeEmail = (e:React.ChangeEvent<HTMLInputElement>):void => {
    const credentials:CredentialsType = {
      email: e.target.value,
      password: cred.password
    };

    setCred(credentials);
    onChangeInput(credentials);
  }

  /** Sets the password value on change */
  const onChangePassword = (e:React.ChangeEvent<HTMLInputElement>):void => {
    const credentials:CredentialsType = {
      email: cred.email,
      password: e.target.value
    };

    setCred(credentials);
    onChangeInput(credentials);
  }

  /** Sets the remember me value on change */
  const onChangeRememberMe = (e:React.ChangeEvent<HTMLInputElement>):void => {
    setRememberMe(e.target.checked);
  }

  /** Handles the submit action */
  const onSubmit = (e:React.FormEvent<HTMLFormElement>):void => {
    e.preventDefault();

    onLogin({
      email: cred.email,
      password: cred.password
    }, rememberMe);
  }

  /** Returns the error message element */
  const renderError = ():JSX.Element => (
    <Typography variant="body1" className={classes.Error}>
      { error }
    </Typography>
  )

  return(
    <Container 
      component="main" 
      maxWidth="sm" 
      className={classes.Login}>

      <img src="./spacex-logo.svg" alt="logo" className={classes.Logo} />

      <form onSubmit={onSubmit} className={classes.LoginCard}>

        <Typography variant="h5">
          Sign in
        </Typography>

        { error ? renderError() : null }

        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email"
          name="email"
          autoComplete="email"
          autoFocus
          value={cred.email} 
          onChange={onChangeEmail}
        />

        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
          value={cred.password} 
          onChange={onChangePassword}
        />

        <FormControlLabel
          control={
            <Checkbox 
              value="remember" 
              color="primary" 
              checked={rememberMe} 
              onChange={onChangeRememberMe} />
          }
          label="Remember me"
        />

        <Button 
          variant="contained" 
          color="primary" 
          type="submit">
          Log In
        </Button>

      </form>

    </Container>
  );
}

export default Login;