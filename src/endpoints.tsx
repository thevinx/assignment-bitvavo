import axios, { AxiosInstance } from 'axios';

const baseURL: AxiosInstance = axios.create({
  baseURL: "https://api.spacexdata.com/v4"
});

export default baseURL;