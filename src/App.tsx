import { BrowserRouter } from 'react-router-dom';
import { createMuiTheme, CssBaseline, ThemeProvider } from '@material-ui/core';
import { orange, deepOrange } from "@material-ui/core/colors";
import Auth from './Components/auth/Auth';
import './App.css';

const App = () => {
  const darkTheme = createMuiTheme({
    palette: {
      type: "dark",
      primary: {
        main: orange[500]
      },
      secondary: {
        main: deepOrange[900]
      }
    }
  });

  return (
    <BrowserRouter>
      <ThemeProvider theme={darkTheme}>
        <CssBaseline />
        <div className="App">
          <Auth />
        </div>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
